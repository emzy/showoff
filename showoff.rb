#!/home/michael/.rvm/rubies/ruby-2.1.0/bin/ruby

require 'sinatra'
require 'slim'
require 'sass'
require 'coffee-script'
require 'json'

require_relative 'lib/video/video'

module ShowOff

  class SassHandler < Sinatra::Base
      
  	set :views, File.dirname(__FILE__) + '/assets/sass'
      
    get '/css/*.css' do
      filename = params[:splat].first
      sass filename.to_sym
    end
      
  end

  class CoffeeHandler < Sinatra::Base
    set :views, File.dirname(__FILE__) + '/assets/coffeescript'
    
    get "/js/*.js" do
      filename = params[:splat].first
      coffee filename.to_sym
    end
  end
   
  class App < Sinatra::Base
    use SassHandler
    use CoffeeHandler

    set :public_dir , File.dirname(__FILE__) + '/public'
    set :views, File.dirname(__FILE__) + '/views'
    set :media_dir, ENV['MEDIA'] || File.dirname(__FILE__) + '/media'
    set :supported_types, ENV['types'] || ['mkv','mp4']
      
    get '/' do
      slim :index
    end

  	get '/media.json' do
      content_type "application/json"
      media = Dir.glob("#{settings.media_dir}/**/*.{#{settings.supported_types.join(',')}}")
        .map{|filepath| Video.load_from_path filepath }
        .to_json
  	end

  end

end

if __FILE__ == $0
  ShowOff::App.run! :port => 4567
end
