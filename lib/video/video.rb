require 'ostruct'

module ShowOff

  class Video < OpenStruct

    def self.load_from_path(path)
      attributes = {}
      attributes[:filename] = File.basename path
      attributes[:type] = "tv" if path.include? "/tv/"
      attributes[:type] = "movie" if path.include? "/movies/"
      attributes[:type] = "music" if path.include? "/music/"
      Video.new attributes
    end

    def to_json(options={})
      @table.to_json options
    end
  end

end